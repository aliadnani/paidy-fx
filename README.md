# Paidy Local Forex Proxy

## Dependencies

- JDK 11+
- Scala/SBT

## Running Locally

### Run Application

```bash
# Starts up OneFrame API dependency
docker run -p 8081:8080 --name one-frame -d --rm paidyinc/one-frame

# Run application
sbt run

# Teardown OneFrame API dependency
docker kill one-frame
```
### Run tests

```bash
# Make sure OneFrame is not running - otherwise ports wil conflict
docker kill one-frame

# Run tests
sbt test
```

## High Level Implementation Details

- The application generates all the possible currency pairs (72 in total) from the currently supported **9** currencies
- A list of rates is then fetched for the 72 currency pairs all in one `GET` request on a scheduler (set to fetch every 2 minutes).
- List of fetched rates is then stored in an in-memory cache
- Requests to the `proxy application` will always retrieve from the cache.

## Assumptions and improvements

### Assumes only **9** currency supported currencies
**9** currencies will yield **72** possible currency pairs - in which fetching all in one API request is reasonable.

This however, does not scale well. If the requirement changes to support **25** currencies - **600** pairs will be yielded; further, if all **278** currencies from `ISO 4217` were to be supported, **77006** pairs would be yielded, all of which can not all be feasibly retrieved in one `GET` request. 

The current implementation works well with **9 - 15** supported currencies (personal estimate) - any further, then more intelligent `OneFrame API` fetching strategies should be investigated.

### Time taken to make the first API rates fetch request is longer than time taken to start HTTP server

Hence, there is a ~500ms time window in which requests to `/rates` will fail as the cache has not been populated with rates yet. There are two possible fixes:
- Make the HTTP Server instantiation dependant on the cache having been populated with rate from `OneFrame`
- Implement a `/health` & `/liveness` endpoint to let downstream services know when the forex proxy is up and running 

### OneFrame still returns 200 even on errors (e.g. quota reached, invalid currency pair)

Response body should be conditionally parsed and throw errors accordingly

### Account for possible OneFrame API flakiness

Implement `cats-retry` for API requests

### Better logging

Things like:
- When the scheduler fetches from OneFrame API
- Errors

**should be logged**

### Improve tests

1. Better use of `TestContainers`
   1. Remove fixed host port container 
2. Test specific configs
3. More tests