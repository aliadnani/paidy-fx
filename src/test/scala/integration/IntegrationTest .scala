package integration

import sttp.client3.HttpClientSyncBackend
import sttp.client3.SttpBackend
import sttp.client3._
import sttp.client3.basicRequest
import pureconfig.ConfigSource
import forex.config.ApplicationConfig
import pureconfig.generic.auto._
import forex.domain.Rate
import sttp.client3.circe.asJson

import io.circe.generic.auto._
import forex.domain.Currency
import scala.util.Random
import org.scalatest.funsuite.AnyFunSuite
import java.time.OffsetDateTime
import scala.collection.parallel.CollectionConverters._

import forex.Main
import com.dimafeng.testcontainers.ForAllTestContainer
import com.dimafeng.testcontainers.FixedHostPortGenericContainer

class IntegrationTest extends AnyFunSuite with ForAllTestContainer {

  val container = FixedHostPortGenericContainer("paidyinc/one-frame",
    exposedHostPort = 8081,
    exposedContainerPort = 8080,
    exposedPorts = Seq(8080)
  )

  val client: SttpBackend[Identity, Any] = HttpClientSyncBackend()

  val config = ConfigSource.default.at("app").loadOrThrow[ApplicationConfig]

  case class Response(
      from: String,
      to: String,
      price: BigDecimal,
      timestamp: OffsetDateTime
  )

  def requestRate(pair: Rate.Pair) = {

    val uri = uri"http://${config.http.host}:${config.http.port}/rates?${Map("to" -> pair.to, "from" -> pair.from)}"

    val response = basicRequest
      .header("Token", "10dc303535874aeccc86a8251e6992f5")
      .get(uri)
      .response(asJson[Response])
      .send(client)

    response.body
  }
  test("Integration test") {

    Main.run(List.empty).unsafeRunAsyncAndForget() match {
      case _ =>
        // Arbitary sleep to wait for application to load
        Thread.sleep(3000)

        // Safety factor of 1.5
        (1 to 15000).par.foreach { _ =>
          val pair = Random.shuffle(Currency.allPairs).head

          requestRate(pair) match {
            case Right(rate) =>
              assert(rate.from === pair.from.toString())
              assert(rate.to === pair.to.toString())
              assert(rate.price !== null)

            case Left(cause) => assert(false, cause)
          }
        }
    }
  }
}
