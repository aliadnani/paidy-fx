package forex.domain

case class Rate(
    pair: Rate.Pair,
    price: Price,
    timestamp: Timestamp
)

object Rate {
  case class Pair(
      from: Currency,
      to: Currency
  ) {
    override def toString() = s"$from$to"
  }
}
