package forex.services.cache

import forex.services.cache.interpreters.InMemoryCacheLive
import cats.effect.concurrent.Ref
import forex.domain.Rate
import cats.effect.Concurrent

object Interpreters {
  def inMemoryCacheLive[F[_]: Concurrent](cache: Ref[F, Map[Rate.Pair, Rate]]): Algebra[F] =
    new InMemoryCacheLive[F](cache)
}
