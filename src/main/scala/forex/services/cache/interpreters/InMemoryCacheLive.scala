package forex.services.cache.interpreters

import forex.services.cache.Algebra
import cats.effect.concurrent.Ref
import cats.implicits._
import forex.domain.Rate
import cats.effect.Concurrent

class InMemoryCacheLive[F[_]: Concurrent](cache: Ref[F, Map[Rate.Pair, Rate]]) extends Algebra[F] {
  def get(pair: Rate.Pair): F[Option[Rate]] = cache.get.map(c => c.get(pair))

  def setMany(rates: Map[Rate.Pair, Rate]): F[Unit] = cache.set(rates)

}
