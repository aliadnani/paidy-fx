package forex.services.cache

import forex.domain.Rate

trait Algebra[F[_]] {
  def get(pair: Rate.Pair): F[Option[Rate]]

  def setMany(rates: Map[Rate.Pair, Rate]): F[Unit]
}
