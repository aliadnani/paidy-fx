package forex.services.scheduled

import fs2.Stream
import scala.concurrent.duration.FiniteDuration
import forex.services.rates.errors.Error

trait Algebra[F[_]] {
  def tasks(): Stream[F,(Either[Error,Unit], FiniteDuration)]
}
