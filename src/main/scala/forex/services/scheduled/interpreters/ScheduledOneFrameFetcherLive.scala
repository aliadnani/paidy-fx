package forex.services.scheduled.interpreters

import forex.services.scheduled.Algebra
import forex.services.rates.errors.Error
import forex.domain.Currency
import fs2.Stream
import cats.implicits._
import forex.services._
import scala.concurrent.duration.FiniteDuration
import cats.data.EitherT
import cats.effect.Concurrent
import cats.effect.Timer
import forex.config.OneFrameConfig

class ScheduledOneFrameFetcherLive[F[_]: Concurrent: Timer](
    config: OneFrameConfig,
    ratesService: RatesService[F],
    cacheService: CacheService[F])
    extends Algebra[F] {

  override def tasks(): fs2.Stream[F, (Either[Error, Unit], FiniteDuration)] = Stream(()).repeat
    .evalMap(_ =>
      {
        for {
          allRates <- EitherT(ratesService.getAll(Currency.allPairs))
          _ <- EitherT(cacheService.setMany(allRates).map(_.asRight[Error]))
        } yield ()
      }.value)
    .zip(Stream.awakeEvery[F](config.fetchInterval))
}
