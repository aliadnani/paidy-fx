package forex.services.scheduled

import forex.services._
import cats.effect.Concurrent
import forex.config.OneFrameConfig
import forex.services.scheduled.interpreters._
import cats.effect.Timer

object Interpreters {
  def live[F[_]: Concurrent: Timer](
      config: OneFrameConfig,
      ratesService: RatesService[F],
      cacheService: CacheService[F]): Algebra[F] = new ScheduledOneFrameFetcherLive(config, ratesService, cacheService)
}
