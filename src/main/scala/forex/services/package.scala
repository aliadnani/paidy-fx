package forex

package object services {
  type RatesService[F[_]] = rates.Algebra[F]
  final val RatesServices = rates.Interpreters
  type CacheService[F[_]] = cache.Algebra[F]
  final val CacheServices = cache.Interpreters
  type ScheduledService[F[_]] = scheduled.Algebra[F]
  final val ScheduledServices = scheduled.Interpreters
}
