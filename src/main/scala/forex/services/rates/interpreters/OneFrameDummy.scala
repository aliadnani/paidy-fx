package forex.services.rates.interpreters

import forex.services.rates.Algebra
import cats.Applicative
import cats.syntax.applicative._
import cats.syntax.either._
import forex.domain.{ Price, Rate, Timestamp }
import forex.services.rates.errors._
import forex.domain.Currency

class OneFrameDummy[F[_]: Applicative] extends Algebra[F] {

  override def get(pair: Rate.Pair): F[Error Either Rate] =
    Rate(pair, Price(BigDecimal(100)), Timestamp.now).asRight[Error].pure[F]

  override def getAll(rates: List[Rate.Pair]): F[Error Either Map[Rate.Pair, Rate]] =
    Currency.allPairs
      .map(p => (p, Rate(p, Price(BigDecimal(100)), Timestamp.now)))
      .toMap
      .asRight[Error]
      .pure[F]

}
