package forex.services.rates.interpreters

import forex.services.rates.Algebra
import forex.domain.{ Price, Rate, Timestamp }
import sttp.client3.SttpBackend
import sttp.client3.basicRequest
import sttp.client3.circe.asJson
import sttp.client3.UriContext
import io.circe.generic.auto._
import java.time.OffsetDateTime
import forex.services.rates.errors.Error
import forex.domain.Currency
import forex.config.OneFrameConfig
import cats.implicits._
import cats.effect.Async
import forex.services._

case class ResponsePayload(
    from: String,
    to: String,
    bid: BigDecimal,
    ask: BigDecimal,
    price: BigDecimal,
    time_stamp: OffsetDateTime) {

  def toRate: Either[Error, Rate] =
    (Currency.fromString(from), Currency.fromString(to)) match {
      case (Right(f), Right(t)) =>
        Right(
          Rate(Rate.Pair(f, t), Price(price), Timestamp(time_stamp))
        )
      case _ => Left(Error.OneFrameMalformedResponse(f"Currency pair `${from} <-> ${to}` not supported."))
    }
}
case class ResponseErrorPayload(error: String) {}

class OneFrameLiveHttp[F[_]: Async](backend: SttpBackend[F, _], oneFrameConfig: OneFrameConfig, cache: CacheService[F])
    extends Algebra[F] {

  private def mkRequest(pairs: List[Rate.Pair]) = {

    val paramsTuple = pairs.map(p => ("pair", p.toString()))

    val uri = uri"${oneFrameConfig.host}/rates"
      .addParams(paramsTuple: _*)

    basicRequest
      .header("Token", "10dc303535874aeccc86a8251e6992f5")
      .get(uri)
      .response(asJson[List[ResponsePayload]])
  }
  override def get(pair: Rate.Pair): F[Error Either Rate] =
    for {
      rateOpt <- cache.get(pair)
      rate = rateOpt.toRight(Error.Unknown())
    } yield rate

  override def getAll(rates: List[Rate.Pair]): F[Error Either Map[Rate.Pair, Rate]] = for {
    response <- backend.send(mkRequest(rates))
    body = response.body.leftMap(e => Error.OneFrameLookupFailed(e.toString()))
    allRates = body.flatMap { rate =>
                 val (lefts, right) = rate.map(_.toRate).partitionMap(identity)
                 lefts.headOption
                   .toLeft(right.map(r => (r.pair, r)).toMap)
               }
  } yield allRates
}
