package forex.services.rates

import forex.domain.Rate
import errors._

trait Algebra[F[_]] {
  def get(pair: Rate.Pair): F[Error Either Rate]
  def getAll(rates: List[Rate.Pair]): F[Error Either Map[Rate.Pair, Rate]]
}
