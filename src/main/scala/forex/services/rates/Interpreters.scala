package forex.services.rates

import cats.Applicative
import interpreters._
import sttp.client3.SttpBackend
import forex.config.OneFrameConfig
import cats.effect.Async
import forex.services._

object Interpreters {
  def dummy[F[_]: Applicative]: Algebra[F] = new OneFrameDummy[F]
  def live[F[_]: Async](
      backend: SttpBackend[F, _],
      oneFrameConfig: OneFrameConfig,
      cache: CacheService[F]): Algebra[F] = new OneFrameLiveHttp[F](backend, oneFrameConfig, cache)
}
