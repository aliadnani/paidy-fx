package forex

import cats.effect.{ Concurrent, Timer }
import forex.config.ApplicationConfig
import forex.http.rates.RatesHttpRoutes
import forex.services._
import forex.programs._
import org.http4s._
import org.http4s.implicits._
import org.http4s.server.middleware.{ AutoSlash, Timeout }
import sttp.client3.SttpBackend
import cats.effect.concurrent.Ref
import forex.domain.Rate


class Module[F[_]: Concurrent: Timer](
    config: ApplicationConfig,
    backend: SttpBackend[F, _],
    cache: Ref[F, Map[Rate.Pair, Rate]]) {

  private val cacheService: CacheService[F] = CacheServices.inMemoryCacheLive[F](cache)

  private val ratesService: RatesService[F] = RatesServices.live[F](backend, config.oneFrame, cacheService)

  private val ratesProgram: RatesProgram[F] = RatesProgram[F](ratesService)

  private val ratesHttpRoutes: HttpRoutes[F] = new RatesHttpRoutes[F](ratesProgram).routes

  val scheduledRatesFetcher: ScheduledService[F] =
    ScheduledServices.live[F](config.oneFrame, ratesService, cacheService)

  type PartialMiddleware = HttpRoutes[F] => HttpRoutes[F]
  type TotalMiddleware   = HttpApp[F] => HttpApp[F]

  private val routesMiddleware: PartialMiddleware = { http: HttpRoutes[F] =>
    AutoSlash(http)
  }

  private val appMiddleware: TotalMiddleware = { http: HttpApp[F] =>
    Timeout(config.http.timeout)(http)
  }

  private val http: HttpRoutes[F] = ratesHttpRoutes

  val httpApp: HttpApp[F] = appMiddleware(routesMiddleware(http).orNotFound)

}
